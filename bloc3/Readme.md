Bloc 3 - Architectures matérielles et robotique, systèmes et réseaux
====================================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Intervenants - contacts
=======================

Benoit Papegay,
Jean-François Roos,
Jean-Luc Levaire,
Laurent Noé,
Philippe Marquet,
Yvan Peter

Et
Gilles Grimaud

Mais aussi
Pierre Boulet,
Thomas Vantroys,
Yann Secq

Jeudi 9 mai 2019
================

Prise en main de l'environnement
--------------------------------

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Très et trop brève introduction
  - Découverte de l'interpréteur de commandes
* [GitLab](seance0/gitlab.md)
  - Découverte de l'environnement GitLab
* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Utiliser l'interpréteur de commandes
  - ...

Vendredi 10 mai 2019
====================

Architecture des machines informatiques et des systèmes d'exploitation
----------------------------------------------------------------------

conférence de Gilles Grimaud, _Turing, von Neumann, ... architecture des machines
informatiques et des systèmes d'exploitation_

* Machine de Turing
  [page Wikipedia fr](https://fr.wikipedia.org/wiki/Machine_de_Turing)
* article fondateur d'Alan Turing, 1936 _« On Computable Numbers, with
  an Application to the Entscheidungsproblem »_
 - notes 10 et 11 de la page Wikipedia [Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing#cite_note-on_computable_numbers-10) 
* _Le modèle d’architecture de von Neumann_ par Sacha Krakowiak, sur
  [interstices.info/](https://interstices.info/le-modele-darchitecture-de-von-neumann/)


Machine virtuelle
-----------------

* [Installation de VirtualBox](virtualbox/seance-virtualisation.md)
* [Conclusions, rappel des objectifs](virtualbox/seance-virtualisation.md#conclusions)

Mercredi 5 juin 2019
====================

Système d'exploitation
----------------------

* « _Système d'exploitation — système de fichiers,
  processus, shell_ »
  * support de présentation
	[4 pages par page](psfssh/psfssh-4up.pdf) /
	[1 page par page](psfssh/psfssh-slide.pdf) /
	[source Markdown](psfssh/psfssh.md)

Machine virtuelle
-----------------

* [Conclusions, rappel des objectifs](virtualbox/seance-virtualisation.md#conclusions)

Interpréteur de commandes
-------------------------

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - (suite et fin...)

Travail pour le 19 juin 2019
============================

* [Fiche de cours, d'activité sur les aspects systèmes](seance0/fiche-systeme.md)

Mercredi 19 juin 2019
=====================

Réseau
------

* [Introduction aux réseaux informatiques](reseau2/network-slides.pdf)
  - cours 
* [Première manip réseau](reseau2/manipulation.md)
  - travaux pratiques, 

Jeudi 20 juin 2019
==================

Système
-------

* « _Système d'exploitation et logiciels libres_ »
  
	> _Les différences entre systèmes d’exploitation libres et
	> propriétaires sont évoquées. \
	> Les élèves utilisent un système d’exploitation libre._

  - support de présentation _Informatique libre_ 
	[4 pages par page](libre/libertes-papier.pdf) / 
	[1 page par page](libre/libertes-ecran.pdf)

* Retour sur la [Fiche de cours, d'activité sur les aspects systèmes](seance0/fiche-systeme.md)

Réseau
------

* Travaux pratiques, suite et fin
  * [Capture de paquets réseaux](reseau2/reseaux2-capture.md)
  * [Création de machines virtuelles pour VirtualBox](reseau2/creer-vm.md)

* [Activités débranchées autour des protocoles réseaux](reseau2/debranche.md)

Ressources
==========

* Premiers pas en Markdown
  - [Markdown zéro](markdown0/readme.md)

* Carte de référence Unix de Moïse Valvassori
  * [unix-refcard.pdf sur www.ai.univ-paris8.fr](http://www.ai.univ-paris8.fr/~djedi/poo/unix-refcard.pdf)
  * [copie locale](doc/unix-refcard.pdf)


