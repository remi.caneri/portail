Tri sans ordinateur - séance 1 - déroulement
============================================

A- En salle TD : 1h30
---------------------

1. Présentation rapide du contenu, des objectifs et du déroulement des
   séances du bloc 2 (5 min) 
   - apporter des éléments théoriques sur l'algorithmique, la
     complexité et les preuves en particulier 
   - axé sur les tris
   - chaque séance peut être refaite avec les élèves
   - échanges souhaités sur leurs attentes au cours des séances pour
     ajuster ce qu'on leur propose 

2. Tri du groupe : les participants reçoivent comme consigne de se
   trier sans plus de précision 
   - deux notions doivent émerger : comparateur et sens du tri
   - ne surtout pas donner de comparateur dans la consigne !!!

3. Organisation de la salle pour les ateliers, a priori 6 sous groupes
   de 4 par groupe 

4. Activité avec les boîtes "tous différents" :
   1. Matériel par sous groupe : 
	  * une enveloppe contenant des boîtes de poids tous différents
      * une règle
	  * un crayon
	  * feuilles A3 (idéalement A2)
   2. Consigne : à donner en plusieurs fois
	  * trier les boîtes par ordre croissant
	  * formaliser ensuite par écrit et en français la méthode
		trouvée : les indications doivent être suffisamment claires pour
		que la méthode soit reproductible par n'importe qui sans
		explications supplémentaires 
      * vérifier que la méthode est clairement décrite en la déroulant
		en suivant étape par étape ce qu'ils ont écrit, et en profiter
		pour compter les comparaisons réalisées
      * faire valider la méthode par les intervenants
	  * recommencer pour élaborer une méthode différente
	  * faire un poster format A3 sur l'un des tris trouvé
	  * expliquer l'une des méthodes à tout le groupe à la fin de l'activité
   3. Pour les intervenants :
	  * ne pas nommer les tris trouvés
	  * le tri par insertion est le plus fréquemment trouvé
	  * s'assurer qu'au moins 4 tris différents sont présentés à la
        fin : ne pas laisser les participants choisir seuls ce qu'ils
        présentent 
	  * pour le 2e tri les sous groupes auront besoin d'être plus
        guidés : ne pas les aiguiller sur le tri par sélection ni sur
        le tri bête qui seront vus en TP
	  * lors des présentations faire remarquer que les groupes ont
        produit des descriptions différentes pour une même méthode : \
        6 groupes présentants 4 tris => au moins un tri est présenté deux fois

5. Activité avec les boîtes "au plus un différent" :
   1. Matériel par sous groupe :
	  * une enveloppe contenant des boîtes dont au plus une est
        différente des autres 
	  * une règle
      * un crayon
   2. Consigne :
	  * déterminer avec le moins de pesées possible s'il y a une boîte
        différente et laquelle est ce 
      * formaliser la méthode s'il reste du temps
   3. Pour les intervenants :
	  * présenter sous forme de concours
      * activité pouvant être refaite en classe avec les élèves pour
        donner des idées supplémentaires aux participants 
      * permet de compléter s'il reste du temps à la fin de l'activité
        précédente 


B- En salle TP : 1h30
---------------------

Reconnaître et programmer des tris 

